package com.wbcollect.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wbcollect.common.kafka.DeadLetterQueueErrorHandler;
import com.wbcollect.common.kafka.KafkaConst;
import com.wbcollect.common.kafka.KafkaTemplateBasedPublisher;
import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;

@Configuration
@Import({KafkaAutoConfiguration.class})
public class KafkaCustomAutoConfiguration {

    @Bean(KafkaConst.SINGLE_KAFKA_LISTENER_CONTAINER_FACTORY)
    public ConcurrentKafkaListenerContainerFactory<?, ?> singleKafkaListenerContainerFactory(ConcurrentKafkaListenerContainerFactoryConfigurer configurer, ConsumerFactory<Object, Object> kafkaConsumerFactory) {
        ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
        configurer.configure(factory, kafkaConsumerFactory);
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.RECORD);
        factory.setBatchListener(false);
        return factory;
    }

    @Bean(KafkaConst.SINGLE_KAFKA_LISTENER_CONTAINER_FACTORY_MANUAL_COMMIT)
    public ConcurrentKafkaListenerContainerFactory<?, ?> singleKafkaListenerContainerFactoryManualCommit(ConcurrentKafkaListenerContainerFactoryConfigurer configurer, ConsumerFactory<Object, Object> kafkaConsumerFactory) {
        ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
        configurer.configure(factory, kafkaConsumerFactory);
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL_IMMEDIATE);
        factory.setBatchListener(false);
        return factory;
    }

    @Bean(KafkaConst.BATCH_KAFKA_LISTENER_CONTAINER_FACTORY)
    public ConcurrentKafkaListenerContainerFactory<?, ?> batchKafkaListenerContainerFactory(ConcurrentKafkaListenerContainerFactoryConfigurer configurer, ConsumerFactory<Object, Object> kafkaConsumerFactory) {
        ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new ConcurrentKafkaListenerContainerFactory<>();
        configurer.configure(factory, kafkaConsumerFactory);
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.BATCH);
        factory.setBatchListener(true);
        return factory;
    }

    @Bean(KafkaConst.DLQ_ERROR_HANDLER)
    public KafkaListenerErrorHandler deadLetterQueueErrorHandler() {
        return new DeadLetterQueueErrorHandler(new ObjectMapper());
    }

    @Bean
    public KafkaTemplateBasedPublisher kafkaTemplateBasedPublisher(KafkaTemplate<String, byte[]> kafkaTemplate, ConfigurableEnvironment env) {
        return new KafkaTemplateBasedPublisher(kafkaTemplate, env);
    }
}
