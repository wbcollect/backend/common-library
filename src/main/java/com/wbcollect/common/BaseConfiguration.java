package com.wbcollect.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.messaging.converter.ByteArrayMessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.validation.beanvalidation.OptionalValidatorFactoryBean;

@Configuration
public class BaseConfiguration {

    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory(ByteArrayMessageConverter messageConverter, OptionalValidatorFactoryBean validator) {
        var dfhmf = new DefaultMessageHandlerMethodFactory();
        dfhmf.setMessageConverter(messageConverter);
        dfhmf.setValidator(validator);
        return dfhmf;
    }

    @Bean
    @Primary
    public OptionalValidatorFactoryBean getOptionalValidatorFactoryBean() {
        return new OptionalValidatorFactoryBean();
    }

    @Bean
    public ByteArrayMessageConverter getByteArrayMessageConverter() {
        return new ByteArrayMessageConverter();
    }
}
