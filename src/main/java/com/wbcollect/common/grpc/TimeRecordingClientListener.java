package com.wbcollect.common.grpc;

import io.grpc.ClientCall;
import io.grpc.Metadata;
import io.grpc.Status;

import java.util.function.BiConsumer;

public class TimeRecordingClientListener<R> extends ClientCall.Listener<R> {

    private final ClientCall.Listener<R> responseListener;
    private final BiConsumer<Status, Long> responseTimestampConsumer;

    protected TimeRecordingClientListener(ClientCall.Listener<R> responseListener, BiConsumer<Status, Long> responseTimestampConsumer) {
        super();
        this.responseListener = responseListener;
        this.responseTimestampConsumer = responseTimestampConsumer;
    }

    @Override
    public void onMessage(R message) {
        responseListener.onMessage(message);
    }

    @Override
    public void onHeaders(Metadata headers) {
        responseListener.onHeaders(headers);
    }

    @Override
    public void onClose(Status status, Metadata trailers) {
        responseTimestampConsumer.accept(status, System.currentTimeMillis());
        responseListener.onClose(status, trailers);
    }

    @Override
    public void onReady() {
        responseListener.onReady();
    }
}
