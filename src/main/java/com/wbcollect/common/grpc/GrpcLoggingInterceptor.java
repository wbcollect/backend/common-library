package com.wbcollect.common.grpc;

import io.grpc.*;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

//Simple class to log gRPC calls with latencies into trace log
@Slf4j
public class GrpcLoggingInterceptor implements ClientInterceptor {

    public static final GrpcLoggingInterceptor DEFAULT_INSTANCE = new GrpcLoggingInterceptor();

    @Override
    public <M, R> ClientCall<M, R> interceptCall(final MethodDescriptor<M, R> method, CallOptions callOptions, Channel next) {
        return new TimingLoggingClientCall<>(method, next.newCall(method, callOptions));
    }

    private static class TimingLoggingClientCall<M, R> extends ForwardingClientCall.SimpleForwardingClientCall<M, R> {
        String methodName;

        protected TimingLoggingClientCall(MethodDescriptor<M, R> method, ClientCall<M, R> delegate) {
            super(delegate);
            methodName = Arrays.stream(method.getFullMethodName().split("\\."))
                    .reduce((s1, s2) -> s2)
                    .orElse(null);
        }

        @Override
        public void start(final ClientCall.Listener<R> responseListener, final Metadata metadata) {
            var startTs = System.currentTimeMillis();
            log.trace("gRPC call start: {}", methodName);
            super.start(new TimeRecordingClientListener<>(responseListener, (status, endTs) -> {
                log.trace("gRPC call {} ended with status {}, took {} ms", methodName, status.getCode(), endTs - startTs);
            }), metadata);
        }
    }
}
