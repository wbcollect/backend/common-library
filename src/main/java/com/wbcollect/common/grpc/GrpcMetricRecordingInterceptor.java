package com.wbcollect.common.grpc;

import io.grpc.*;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GrpcMetricRecordingInterceptor implements ClientInterceptor {

    private final MeterRegistry meterRegistry;

    public GrpcMetricRecordingInterceptor(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Override
    public <M, R> ClientCall<M, R> interceptCall(final MethodDescriptor<M, R> method, CallOptions callOptions, Channel next) {
        return new MetricRecordingClientCall<>(method, next.newCall(method, callOptions), meterRegistry);
    }

    private static class MetricRecordingClientCall<M, R> extends ForwardingClientCall.SimpleForwardingClientCall<M, R> {
        private final Tags tags;
        private final MeterRegistry meterRegistry;

        protected MetricRecordingClientCall(MethodDescriptor<M, R> method, ClientCall<M, R> delegate, MeterRegistry meterRegistry) {
            super(delegate);

            var fullName = method.getFullMethodName();
            var idx = fullName.lastIndexOf('/');
            var methodName = fullName.substring(idx + 1);
            var serviceName = fullName.substring(0, idx);

            tags = Tags.of(
                    Tag.of("service", serviceName),
                    Tag.of("method", methodName),
                    Tag.of("methodType", method.getType().name())
            );
            this.meterRegistry = meterRegistry;
        }

        @Override
        public void start(final Listener<R> responseListener, final Metadata metadata) {
            meterRegistry.counter("grpc.client.requests.sent", tags).increment();
            final var startTs = System.currentTimeMillis();
            super.start(new TimeRecordingClientListener<>(responseListener, (status, endTs) -> {
                meterRegistry.counter("grpc.client.responses.received", tags).increment();
                meterRegistry
                        .summary("grpc.client.processing.duration", tags.and(Tag.of("statusCode", status.getCode().name())))
                        .record(endTs - startTs);
            }), metadata);
        }
    }
}
