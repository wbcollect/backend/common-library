package com.wbcollect.common.grpc;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientInterceptor;
import io.grpc.stub.AbstractStub;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;

@Slf4j
public class StubFactory {

    private final ClientInterceptor[] interceptors;

    public StubFactory(GrpcMetricRecordingInterceptor metricRecordingInterceptor) {
        if (log.isTraceEnabled()) {
            interceptors = new ClientInterceptor[]{GrpcLoggingInterceptor.DEFAULT_INSTANCE, metricRecordingInterceptor};
        } else {
            interceptors = new ClientInterceptor[]{metricRecordingInterceptor};
        }
    }

    public <T extends AbstractStub<T>> T newReactorStub(Class<T> clazz, Channel channel) {
        try {
            var ctor = clazz.getDeclaredConstructor(Channel.class, CallOptions.class);
            ctor.setAccessible(true);
            T stub = ctor.newInstance(channel, CallOptions.DEFAULT);
            return stub.withInterceptors(interceptors);

        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException |
                 IllegalAccessException e) {
            log.error("this should never happen: cannot create stub");
            throw new RuntimeException(e);
        }
    }

    public <T extends AbstractStub<T>> T deadlinedStub(Class<T> stubClass, Channel channel) {
        return deadlined(
                newReactorStub(
                        stubClass, channel
                )
        );
    }

    private <T extends AbstractStub<T>> T deadlined(T originalStub) {
        return originalStub;
    }
}
