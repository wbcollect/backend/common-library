package com.wbcollect.common.grpc;

import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoggingInterceptor implements ServerInterceptor {
    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        log.debug("gRPC call start: method={}", call.getMethodDescriptor().getFullMethodName());
        long startTs = System.currentTimeMillis();
        var result = next.startCall(call, headers);
        long endTs = System.currentTimeMillis();
        log.debug("gRPC call end: method={} executionTime={}", call.getMethodDescriptor().getFullMethodName(), endTs - startTs);
        return result;
    }
}
