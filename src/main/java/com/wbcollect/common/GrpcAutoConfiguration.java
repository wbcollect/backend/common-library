package com.wbcollect.common;

import com.wbcollect.common.grpc.GrpcMetricRecordingInterceptor;
import com.wbcollect.common.grpc.StubFactory;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class GrpcAutoConfiguration {

    @Bean
    public GrpcMetricRecordingInterceptor getGrpcMetricRecordingInterceptor(MeterRegistry meterRegistry) {
        return new GrpcMetricRecordingInterceptor(meterRegistry);
    }

    @Bean
    public StubFactory getAbstractStubFactory(GrpcMetricRecordingInterceptor grpcMetricRecordingInterceptor) {
        return new StubFactory(grpcMetricRecordingInterceptor);
    }

}
