package com.wbcollect.common.kafka;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.MessageOrBuilder;
import com.wbcollect.proto.Topic;
import com.wbcollect.proto.util.ProtoUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;

@RequiredArgsConstructor
@Getter
public class KafkaTemplateBasedPublisher {

    private final KafkaTemplate<String, byte[]> kafkaTemplate;
    private final ConfigurableEnvironment env;

    public String resolveName(String topic){
        return env.resolvePlaceholders(topic);
    }

    public <M extends GeneratedMessageV3 & MessageOrBuilder> ListenableFuture<SendResult<String, byte[]>> send(Topic<M> topic, M data) {
        return kafkaTemplate.send(resolveName(topic.name), ProtoUtils.toByteArrayDelimitedOrThrow(data));
    }

    public ListenableFuture<SendResult<String, byte[]>> send(String topic, String key, byte[] data) {
        return kafkaTemplate.send(resolveName(topic), key, data);
    }

    public ListenableFuture<SendResult<String, byte[]>> send(String topic, byte[] data) {
        return kafkaTemplate.send(resolveName(topic), data);
    }

    public ConsumerRecord<String, byte[]> receive(String topic, int partition, long offset) {
        return kafkaTemplate.receive(resolveName(topic), partition, offset);
    }
}
