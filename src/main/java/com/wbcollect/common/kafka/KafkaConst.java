package com.wbcollect.common.kafka;

public interface KafkaConst {

    String SINGLE_KAFKA_LISTENER_CONTAINER_FACTORY = "singleKafkaListenerContainerFactory";
    String SINGLE_KAFKA_LISTENER_CONTAINER_FACTORY_MANUAL_COMMIT = "singleKafkaListenerContainerFactoryManualCommit";
    String BATCH_KAFKA_LISTENER_CONTAINER_FACTORY = "batchKafkaListenerContainerFactory";
    String DLQ_ERROR_HANDLER = "deadLetterQueueErrorHandler";

}
