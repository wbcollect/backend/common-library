package com.wbcollect.common.kafka;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.kafka.clients.consumer.Consumer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;
import org.springframework.kafka.listener.ListenerExecutionFailedException;
import org.springframework.kafka.listener.adapter.InvocationResult;
import org.springframework.messaging.Message;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.kafka.support.KafkaHeaders.*;

@RequiredArgsConstructor
@Slf4j
public class DeadLetterQueueErrorHandler implements KafkaListenerErrorHandler {

    public static final String DEAD_LETTER_QUEUE_TOPIC = "v1.deadletterqueue.messagedied";

    @Value("${spring.application.name}")
    private String appName;

    @Override
    public Object handleError(Message<?> message, ListenerExecutionFailedException exception) {
        throw new RuntimeException("not possible");
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    @Getter(onMethod_ = @JsonProperty)
    @Builder
    public static class DealLetterPayload {
        private final String payload;
        private final Long offset;
        private final String topicName;
        private final Long time;
        private final String service;
        private final List<String> stackTrace;
        private final String message;
        private final String errorType;
        private final String rawError;
    }

    @Override
    public Object handleError(Message<?> message, ListenerExecutionFailedException exception, Consumer<?, ?> consumer) {
        log.error("an error occurred while handling kafka message", exception);
        try {
            return buildInvocationResultUnsafe(message, exception);
        } catch (Exception e) {
            log.error("unable to build payload for deal letter queue, this is very very bad", e);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private InvocationResult buildInvocationResultUnsafe(Message<?> message, ListenerExecutionFailedException exception) throws JsonProcessingException {
        List<byte[]> payloads = tryCastToList(message.getPayload(), byte[].class);
        var offsets = tryCastToList(message.getHeaders().get(OFFSET), Long.class);
        var topicNames = tryCastToList(message.getHeaders().get(RECEIVED_TOPIC), String.class);
        var times = tryCastToList(message.getHeaders().get(RECEIVED_TIMESTAMP), Long.class);
        var resultList = new ArrayList<DealLetterPayload>(payloads.size());
        for (var i = 0; i < payloads.size(); i++) {
            var dealLetterPayload = DealLetterPayload.builder()
                    .payload(Base64.getEncoder().encodeToString(payloads.get(i)))
                    .offset(offsets.get(i))
                    .topicName(topicNames.get(i))
                    .time(times.get(i))
                    .service(appName)
                    .stackTrace(Arrays.stream(exception.getCause().getStackTrace()).map(StackTraceElement::toString).collect(Collectors.toList()))
                    .message(exception.getCause().getMessage())
                    .errorType(exception.getClass().getSimpleName())
                    .rawError(printException(exception.getCause()))
                    .build();
            resultList.add(dealLetterPayload);
        }
        var jsonData = objectMapper.writeValueAsString(resultList);
        return new InvocationResult(jsonData.getBytes(), new LiteralExpression(DEAD_LETTER_QUEUE_TOPIC), true);
    }

    @SuppressWarnings("unchecked")
    private <T> List<T> tryCastToList(Object o, Class<T> tClass) {
        return o instanceof List ? (List<T>) o : List.of((T) o);
    }


    private String printException(Throwable e) {
        return ExceptionUtils.getStackTrace(e);
    }

    private final ObjectMapper objectMapper;
}
